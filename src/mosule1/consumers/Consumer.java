package mosule1.consumers;

import mosule1.consumers.purse.bank_card.BankCard;
import mosule1.consumers.purse.cash.Cash;
import mosule1.starbucks.ICustomersStarbucks;

public class Consumer implements ICustomersStarbucks {
    private String name;
    private int numberCoffee;
    private Cash cash;
    private BankCard card;
    private boolean flagPassagePayment;

    public Consumer(String name, int numberCoffee, Cash cash, BankCard card) {
        this.name = name;
        this.numberCoffee = numberCoffee;
        this.cash = cash;
        this.card = card;
        flagPassagePayment = dropTheCoin();
    }

    @Override
    public int getNumberCoffee() {
        return numberCoffee;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setCash(int newCash) {
        cash.setBalance(newCash);
    }

    @Override
    public int getCash() {
        return cash.getBalance();
    }

    @Override
    public void setCard(int newBalance) {
        card.setBalance(newBalance);
    }

    @Override
    public int getCard() {
        return card.getBalance();
    }

    @Override
    public IPaymentMethod choicePayment() {
        if (flagPassagePayment) {
            return card;
        } else {
            return cash;
        }
    }

    @Override
    public void setFlagPassagePayment(boolean passagePayment) {
        flagPassagePayment = passagePayment;
    }

    @Override
    public boolean getFlagPassagePayment() {
        return flagPassagePayment;
    }


    private boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }
}
