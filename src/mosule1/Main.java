package mosule1;

import mosule1.consumers.Consumer;
import mosule1.consumers.purse.bank_card.BankCard;
import mosule1.consumers.purse.cash.Cash;
import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.Starbucks;
import mosule1.starbucks.bar_counters.IOrderExecutors;

public class Main {

    public static void main(String[] args) {
        ICustomersStarbucks customer1 = new Consumer("Eva", IOrderExecutors.LATTE, new Cash(100), new BankCard(0));
        ICustomersStarbucks customer2 = new Consumer("Thomas", IOrderExecutors.AMERICANO, new Cash(0), new BankCard(100));
        ICustomersStarbucks customer3 = new Consumer("Jack", IOrderExecutors.ESPRESSO, new Cash(100), new BankCard(0));
        ICustomersStarbucks customer4 = new Consumer("Anna", IOrderExecutors.AMERICANO, new Cash(0), new BankCard(0));
        ICustomersStarbucks customer5 = new Consumer("Linda", IOrderExecutors.LATTE, new Cash(20), new BankCard(0));
        ICustomersStarbucks customer6 = new Consumer("Daniel", IOrderExecutors.AMERICANO, new Cash(0), new BankCard(30));
        ICustomersStarbucks customer7 = new Consumer("Jessie", IOrderExecutors.LATTE, new Cash(50), new BankCard(0));
        ICustomersStarbucks customer8 = new Consumer("Betty", IOrderExecutors.ESPRESSO, new Cash(0), new BankCard(50));


        Starbucks starbucks = new Starbucks(
                customer1, customer2, customer3, customer4, customer5, customer6, customer7, customer8);
        starbucks.open();
    }
}
