package mosule1.starbucks.cashboxes;

import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CardException;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CashException;

public interface ICalculationMethod {
    void transaction(ICustomersStarbucks customer, int purchaseAmount) throws CardException, CashException;
}
