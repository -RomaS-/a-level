package mosule1.starbucks.cashboxes;

import mosule1.consumers.IPaymentMethod;
import mosule1.consumers.purse.bank_card.BankCard;
import mosule1.consumers.purse.cash.Cash;
import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.IWorkWithCashbox;
import mosule1.starbucks.bar_counters.IOrderExecutors;
import mosule1.starbucks.cashboxes.types_of_payment.cash_payment.CashPayment;
import mosule1.starbucks.cashboxes.types_of_payment.terminals_payment.Terminal;
import mosule1.starbucks.exception.cashbox_exceptions.CashboxException;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CardException;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CashException;

public class Cashbox implements IWorkWithCashbox {
    private static int income;
    private String strOffer = "";
    private ICalculationMethod cash;
    private ICalculationMethod terminal;

    public Cashbox() {
        cash = new CashPayment();
        terminal = new Terminal();
    }

    @Override
    public void payment(ICustomersStarbucks customer) throws CashboxException {
        int purchaseAmount = calculationPurchasePrice(customer);
        moneyTransaction(customer, purchaseAmount);
        calculationProfits(purchaseAmount);
        printStatistics(customer, purchaseAmount);
    }

    private int calculationPurchasePrice(ICustomersStarbucks customer) {
        switch (customer.getNumberCoffee()) {
            case IOrderExecutors.LATTE:
                return IOrderExecutors.PRICE_LATTE;
            case IOrderExecutors.AMERICANO:
                return IOrderExecutors.PRICE_AMERICANO;
            case IOrderExecutors.ESPRESSO:
                return IOrderExecutors.PRICE_ESPRESSO;
            default:
                return 0;
        }
    }

    private void moneyTransaction(ICustomersStarbucks customer, int purchaseAmount) throws CashboxException {
        try {
            getTypePayment(customer).transaction(customer, purchaseAmount);
        } catch (CashException | CardException e) {
            System.out.println(e.getMessage() + strOffer);
            customer.setFlagPassagePayment(!customer.getFlagPassagePayment());
            moneyTransaction(customer, purchaseAmount);
        }
    }

    private void calculationProfits(int purchaseAmount) {
        income += purchaseAmount;
    }

    private void printStatistics(ICustomersStarbucks customer, int purchaseAmount) {
        System.out.println("Payment operation was successful. " + customer.getName() + " spend " + purchaseAmount + "$" +
                " \nThe income per day is " + income + "$");
    }

    private ICalculationMethod getTypePayment(ICustomersStarbucks customer) {
        IPaymentMethod method = customer.choicePayment();
        if (method instanceof BankCard) {
            strOffer = " May be cash?";
            return terminal;
        } else if (method instanceof Cash) {
            strOffer = " May be Bank Card?";
            return cash;
        }
        throw new CashboxException("Unknown payment method!");
    }
}
