package mosule1.starbucks.cashboxes.types_of_payment.terminals_payment;

import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.cashboxes.types_of_payment.BaseTypesPayment;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CardException;

public class Terminal extends BaseTypesPayment {
    @Override
    public void transaction(ICustomersStarbucks customer, int purchaseAmount) throws CardException {

        if (isMoneyForPayment(customer, purchaseAmount)) {
            int newCash = customer.getCard() - purchaseAmount;
            customer.setCard(newCash);
        } else {
            throw new CardException("The balance on the card is not enough!");
        }
    }

    @Override
    protected boolean isMoneyForPayment(ICustomersStarbucks customer, int purchaseAmount) {
        if (super.isMoneyForPayment(customer, purchaseAmount)) {
            printTypePayment("Bank Card");
            return customer.getCard() >= purchaseAmount;
        }
        return false;
    }
}
