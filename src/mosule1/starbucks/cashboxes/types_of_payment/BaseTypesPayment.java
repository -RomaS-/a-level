package mosule1.starbucks.cashboxes.types_of_payment;

import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.cashboxes.ICalculationMethod;
import mosule1.starbucks.exception.cashbox_exceptions.CashboxException;

public abstract class BaseTypesPayment implements ICalculationMethod {

    protected boolean isMoneyForPayment(ICustomersStarbucks customer, int purchaseAmount) {
        if (customer.getCash() < purchaseAmount && customer.getCard() < purchaseAmount) {
            throw new CashboxException("Sorry, but you do not have enough money!");
        } else {
            return true;
        }
    }

    protected void printTypePayment(String typePayment) {
        System.out.println("Payment by " + typePayment + ".....");
    }
}
