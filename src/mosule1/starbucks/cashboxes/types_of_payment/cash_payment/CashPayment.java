package mosule1.starbucks.cashboxes.types_of_payment.cash_payment;

import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.cashboxes.types_of_payment.BaseTypesPayment;
import mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException.CashException;

public class CashPayment extends BaseTypesPayment {
    @Override
    public void transaction(ICustomersStarbucks customer, int purchaseAmount) throws CashException {
        if (isMoneyForPayment(customer, purchaseAmount)) {
            int newCash = customer.getCash() - purchaseAmount;
            customer.setCash(newCash);
        } else {
            throw new CashException("Not enough cash to pay!");
        }

    }

    @Override
    protected boolean isMoneyForPayment(ICustomersStarbucks customer, int purchaseAmount) {
        if (super.isMoneyForPayment(customer, purchaseAmount)) {
            printTypePayment("Cash");
            return customer.getCash() >= purchaseAmount;
        } else {
            return false;
        }
    }
}
