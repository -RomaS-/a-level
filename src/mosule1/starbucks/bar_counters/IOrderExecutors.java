package mosule1.starbucks.bar_counters;

import mosule1.starbucks.exception.coffee_exceptions.work_exception.BreakageCoffeeMachineException;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public interface IOrderExecutors {
    int LATTE = 1;
    int ESPRESSO = 2;
    int AMERICANO = 3;

    int PRICE_LATTE = 15;
    int PRICE_ESPRESSO = 5;
    int PRICE_AMERICANO = 10;

    void createCoffee(int numberCoffee) throws BreakageCoffeeMachineException, NotEnoughResourcesException;

    void repair();
}
