package mosule1.starbucks.bar_counters;

import mosule1.starbucks.ICustomersStarbucks;
import mosule1.starbucks.IWorkWithBarCounters;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IReplenishResources;
import mosule1.starbucks.bar_counters.coffee.making_coffee.baristas.Barista;
import mosule1.starbucks.bar_counters.coffee.making_coffee.coffee_machines.CoffeeMachine;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.BreakageCoffeeMachineException;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public class BarCounter implements IWorkWithBarCounters {
    private IOrderExecutors coffeeMachine;
    private IOrderExecutors barista;

    public BarCounter() {
        coffeeMachine = new CoffeeMachine();
        barista = new Barista();
    }

    @Override
    public void completeOrder(ICustomersStarbucks consumers) {
        getCoffee(consumers);
    }

    private void getCoffee(ICustomersStarbucks consumers) {
        try {
            issueOrder(coffeeMachine, consumers.getNumberCoffee());
        } catch (BreakageCoffeeMachineException e) {
            replacementMachineOnBarista(consumers, e);
        }
    }

    private void replacementMachineOnBarista(ICustomersStarbucks consumers, BreakageCoffeeMachineException e) {
        System.out.println(e.getMessage() + " Repairs... \nCoffee prepares barista");
        coffeeMachine.repair();
        issueOrder(barista, consumers.getNumberCoffee());
    }

    private void issueOrder(IOrderExecutors executors, int numberCoffee) {
        try {
            executors.createCoffee(numberCoffee);
        } catch (NotEnoughResourcesException e) {
            replenishStocks(e);
        }
    }

    private void replenishStocks(NotEnoughResourcesException e) {
        IReplenishResources resource = e.getResource();
        System.out.println(e.getMessage() + " Barista replenishes...");
        resource.replenish();
    }
}
