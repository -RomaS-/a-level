package mosule1.starbucks.bar_counters.coffee.resources_for_coffee.consumables;

import mosule1.starbucks.IPrintOnCup;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IConsumablesForCoffee;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IReplenishResources;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public class Cup implements IConsumablesForCoffee, IPrintOnCup, IReplenishResources {
    private final int MAX_RESERVE = 20;
    private static int spend = 0;
    private String textOnCup = "";

    @Override
    public void print(String text) {
        textOnCup = text;
    }

    @Override
    public String getPrintOnCup() {
        return textOnCup;
    }

    @Override
    public int getOneUnit() throws NotEnoughResourcesException {
        if (spend < MAX_RESERVE) {
            spend++;
            return 1;
        } else {
            throw new NotEnoughResourcesException(this, "The milk ran out.");
        }
    }

    @Override
    public void replenish() {
        spend = 0;
    }
}
