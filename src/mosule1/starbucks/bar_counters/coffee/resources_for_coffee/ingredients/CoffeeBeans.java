package mosule1.starbucks.bar_counters.coffee.resources_for_coffee.ingredients;

import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IIngredientsForCoffee;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IReplenishResources;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public class CoffeeBeans implements IIngredientsForCoffee, IReplenishResources {
    private final int MAX_RESERVE_G = 500;
    private int spend = 0;

    @Override
    public int getIngredient(int amountPerServing) throws NotEnoughResourcesException {
        int newSpend = spend + amountPerServing;
        if (newSpend <= MAX_RESERVE_G) {
            spend = newSpend;
            return amountPerServing;
        } else {
            throw new NotEnoughResourcesException(this, "The coffee beans ran out.");
        }
    }

    @Override
    public void replenish() {
        spend = 0;
    }


}
