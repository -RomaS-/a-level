package mosule1.starbucks.bar_counters.coffee.kinds_coffe.coffee_pure;

import mosule1.starbucks.bar_counters.coffee.kinds_coffe.BaseKindsCoffee;

public class EspressoClassic extends BaseKindsCoffee {
    private final int COFFEE_PER_SERVING_G = 10;
    private final int WATER_PER_SERVING_ML = 35;

    @Override
    public void mixing() {
        getConsumablesForPortions(cup);
        getIngredientForPortions(coffeeBeans, COFFEE_PER_SERVING_G);
        getIngredientForPortions(water, WATER_PER_SERVING_ML);
    }
}
