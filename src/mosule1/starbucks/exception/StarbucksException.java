package mosule1.starbucks.exception;

public class StarbucksException extends RuntimeException {
    public StarbucksException() {
        super();
    }

    public StarbucksException(String message) {
        super(message);
    }
}
